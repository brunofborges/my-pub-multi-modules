package com.emailmicroservice.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mypub.Drink;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Component
@Slf4j
@RequiredArgsConstructor
public class EmailService {

    private final JavaMailSender javaMailSender;

    @KafkaListener(topics = "drinks", groupId = "group1")
    public void sendEmail(Drink drink) throws MessagingException {
        log.info("Received new Message...");
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "utf-8");
        String htmlMessage =
                "<h3> Name: " + drink.getName() + "</h3>" +
                        "<h4> Ingredients:</h4>" +
                        drink.getIngredients().entrySet().stream()
                                .map(entry -> "<p>" + entry.getKey() + " - " + entry.getValue() + "</p>")
                                .reduce("", String::concat) +
                        "<p> Preparation: " + drink.getPreparation() + "</p>" +
                        "<p> Garnish: " + drink.getGarnish() + "</p>" +
                        "<p> Glass: " + drink.getGlass() + "</p>" +
                        "<p> Cost: " + drink.getCost() + "</p>" +
                        "<p> ABV: " + drink.getAbv() + "</p>";
        helper.setFrom("testes.java.forttiori@gmail.com");
        helper.setTo("testes.java.forttiori@gmail.com");
        helper.setSubject(drink.getEventDescription());
        helper.setText(htmlMessage, true);
        javaMailSender.send(mimeMessage);
        log.info("Email sent successfully");
    }

}
