package com.bruno.mypub.kafkaproducer.service;


import lombok.RequiredArgsConstructor;
import mypub.Drink;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class KafkaProducerService {

    private final KafkaTemplate<String, Drink> kafkaTemplate;

    public void send(Drink drink) {
        kafkaTemplate.send("drinks",
                UUID.randomUUID().toString(), new Drink(
                        drink.getEventDescription(),
                        drink.getName(),
                        drink.getIngredients(),
                        drink.getCost(),
                        drink.getAbv(),
                        drink.getPreparation(),
                        drink.getGlass(),
                        drink.getGarnish()));
    }
}
