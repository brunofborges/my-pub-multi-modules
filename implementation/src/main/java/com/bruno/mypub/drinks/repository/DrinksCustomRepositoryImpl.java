package com.bruno.mypub.drinks.repository;

import com.bruno.mypub.common.exceptions.NotFoundException;
import com.bruno.mypub.common.query.DrinkQuery;
import com.bruno.mypub.drinks.mapper.DrinksServiceMappers;
import com.bruno.mypub.drinks.repository.entities.DrinkEntity;
import com.bruno.mypub.kafkaproducer.service.KafkaProducerService;
import lombok.AllArgsConstructor;
import mypub.Drink;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Objects;

@Component
@AllArgsConstructor
public class DrinksCustomRepositoryImpl implements DrinksCustomRepository {

    private final ReactiveMongoTemplate reactiveMongoTemplate;
    private final KafkaProducerService kafkaProducerService;

    @Override
    public Flux<DrinkEntity> findDrink(DrinkQuery drinkQuery) {
        if (!Objects.equals(drinkQuery.getId(), "0")) {
            Query query = new Query();
            query.addCriteria(Criteria.where("id").is(drinkQuery.getId()));
            return reactiveMongoTemplate.find(query, DrinkEntity.class)
                    .switchIfEmpty(Mono.error(new NotFoundException("Id not found: " + drinkQuery.getId())));
        } else if (!Objects.equals(drinkQuery.getName(), "")) {
            Query query = new Query();
            query.addCriteria(Criteria.where("name")
                    .regex(StringUtils.capitalize(drinkQuery.getName())));
            return reactiveMongoTemplate.find(query, DrinkEntity.class)
                    .switchIfEmpty(Mono.error(new NotFoundException("Name not found: " + drinkQuery.getName())));
        }
        return reactiveMongoTemplate.findAll(DrinkEntity.class);
    }

    @Override
    public Mono<String> customDelete(String id) {
        if (Objects.equals(id, "0")) {
            return reactiveMongoTemplate.dropCollection(DrinkEntity.class)
                    .flatMap(unused -> {
                        return Mono.just("Successfully Dropped Collection.");
                    });
        }
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        return reactiveMongoTemplate.findAndRemove(query, DrinkEntity.class)
                .switchIfEmpty(Mono.error(new NotFoundException("ID not found: " + id)))
                .flatMap(drinkEntity -> {
                    Drink drinkToKafka = DrinksServiceMappers.INSTANCE
                            .drinkEntityToKafka(drinkEntity, "A Drink Has Been Deleted!");
                    kafkaProducerService.send(drinkToKafka);
                    return Mono.just("Successfully deleted " + drinkEntity.getName());
                });

    }
}
