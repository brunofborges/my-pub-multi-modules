package com.bruno.mypub.drinks.service;

import com.bruno.mypub.common.query.DrinkQuery;
import com.bruno.mypub.common.utilities.DoubleFormatter;
import com.bruno.mypub.drinks.mapper.DrinksServiceMappers;
import com.bruno.mypub.drinks.repository.DrinksRepository;
import com.bruno.mypub.drinks.repository.entities.DrinkEntity;
import com.bruno.mypub.ingredients.model.response.IngredientsServiceResponse;
import com.bruno.mypub.kafkaproducer.service.KafkaProducerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mypub.Drink;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class DrinksService {

    private final DrinksRepository drinksRepository;
    private final KafkaProducerService kafkaProducerService;

    public Mono<DrinkEntity> createDrink(DrinkEntity drinkEntity) {
        return Mono.just(drinkEntity)
                .map(drinkEntity1 -> {
                    drinkEntity.setCost(drinkEntity.getIngredients().stream()
                            .mapToDouble(IngredientsServiceResponse::getPrice).sum());
                    drinkEntity.setCost(DoubleFormatter.formatNumber(drinkEntity.getCost()));
                    drinkEntity.getIngredients().forEach(ingredientEntity ->
                            ingredientEntity.setPrice(DoubleFormatter.formatNumber(ingredientEntity.getPrice())));
                    return calculateAbv(drinkEntity);
                }).map(drinkEntity1 -> {
                    Drink drinkToKafka = DrinksServiceMappers.INSTANCE
                            .drinkEntityToKafka(drinkEntity1, "A New Drink Has Been Added!" );
                    kafkaProducerService.send(drinkToKafka);
                    return drinkEntity1;
                }).flatMap(drinksRepository::save);

    }

    public Flux<DrinkEntity> getDrinks(DrinkQuery drinkQuery) {
        return drinksRepository.findDrink(drinkQuery);
    }

    public Mono<DrinkEntity> updateDrink(String id, DrinkEntity drinkEntityToUpdate) {
        return Mono.just(drinkEntityToUpdate)
                .flatMap(drinkEntity -> drinksRepository.findById(id)
                        .map(drinkEntitySaved -> {
                            drinkEntity.setId(drinkEntitySaved.getId());
                            drinkEntity.setCost(DoubleFormatter.formatNumber(drinkEntity.getIngredients().stream()
                                    .mapToDouble(IngredientsServiceResponse::getPrice).sum()));
                            drinkEntity.getIngredients().forEach(ingredientEntity -> ingredientEntity.setPrice(DoubleFormatter.formatNumber(ingredientEntity.getPrice())));
                            return calculateAbv(drinkEntity);
                        })).map(drinkEntity -> {
                    Drink drinkToKafka = DrinksServiceMappers.INSTANCE
                            .drinkEntityToKafka(drinkEntity, "A Drink Has Been Updated!" );
                    kafkaProducerService.send(drinkToKafka);
                    return drinkEntity;
                }).flatMap(drinksRepository::save);

    }

    public Mono<String> deleteDrink(String id) {
        return drinksRepository.customDelete(id);
    }

    private DrinkEntity calculateAbv(DrinkEntity drinkEntity) {
        List<Double> totalAbv = new ArrayList<>();
        List<Integer> totalVolume = new ArrayList<>();
        drinkEntity.getIngredients().forEach(ingredientsServiceResponse ->
                totalVolume.add(ingredientsServiceResponse.getQuantity()));
        drinkEntity.getIngredients().stream().filter(ingredientEntity -> ingredientEntity.getAbv() != null)
                .forEach(ingredientsServiceResponse ->
                        totalAbv.add(ingredientsServiceResponse.getAbv()
                                * ingredientsServiceResponse.getQuantity() / 100));
        drinkEntity.setAbv(DoubleFormatter.formatNumber((totalAbv.stream()
                .reduce(0.0, Double::sum) * 100) / totalVolume.stream().reduce(0, Integer::sum)));
        return drinkEntity;
    }

}
