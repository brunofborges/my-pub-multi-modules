package com.bruno.mypub.drinks.mapper;

import com.bruno.mypub.drinks.repository.entities.DrinkEntity;
import com.bruno.mypub.drinks.model.request.DrinksServiceRequest;
import com.bruno.mypub.drinks.model.response.DrinksServiceResponse;
import mypub.Drink;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.HashMap;
import java.util.Map;

import static com.fasterxml.jackson.databind.type.LogicalType.Map;

@Mapper
public interface DrinksServiceMappers {

    DrinksServiceMappers INSTANCE = Mappers.getMapper(DrinksServiceMappers.class);

    default Drink drinkEntityToKafka(DrinkEntity drinkEntity, String eventDescription) {
        Map<String, Integer> ingredientsMap = new HashMap<>();
        drinkEntity.getIngredients()
                .forEach(ingredient -> ingredientsMap.put(ingredient.getName(), ingredient.getQuantity()));
        return Drink.newBuilder()
                .setName(drinkEntity.getName())
                .setIngredients(ingredientsMap)
                .setCost(drinkEntity.getCost())
                .setAbv(drinkEntity.getAbv())
                .setEventDescription(eventDescription)
                .setPreparation(drinkEntity.getPreparation())
                .setGlass(drinkEntity.getGlass())
                .setGarnish(drinkEntity.getGarnish())
                .build();
    }
    DrinksServiceResponse drinkEntityToResponse(DrinkEntity entity);

    DrinkEntity drinkRequestToEntity(DrinksServiceRequest request);

}
